/*==============================*/

		function addition (num1, num2){
			let sum = num1 + num2;
			console.log ("Displayed sum of " + num1 + " and " + num2);
			console.log(sum);
		}
		addition (5, 15);		

		function subtraction (num1, num2){
			let difference = num1 - num2;
			console.log ("Displayed difference of " + num1 + " and " + num2);
			console.log(difference);
		}
		subtraction (20, 5);

/*==============================*/

		function multiplication (num1, num2){
			let product = num1 * num2;
			console.log ("The product of " + num1 + " and " + num2);
			return product;
		}
		let productResult = multiplication(50, 10);
		console.log(productResult);

		function division (num1, num2){
			let quotient = num1 / num2;
			console.log ("The quotient of " + num1 + " and " + num2);
			return quotient;
		}
		let quotientResult = division(50, 10);
		console.log(quotientResult);

/*==============================*/

		let circleArea = 15;

		function calculation(){
			let squareRoot = Math.pow(circleArea,2);
			let areaOfCirlce = squareRoot * 3.14;
			console.log("The result of getting the area of a circle with " + circleArea +" radius:" );
			console.log(areaOfCirlce);
		}
		calculation();

/*==============================*/

/*	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.*/

		function aveCalculation (num1, num2, num3, num4){
			sumAve = num1 + num2 + num3 + num4;
			average = sumAve/4;
			console.log("The average of "+num1+","+num2+","+num3+" and "+ num4+":");
			return average;
		}
		let averageVar = aveCalculation(20, 40, 60, 80);
		console.log(averageVar);

/*	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.*/

		function thePassingScore (num1, num2){
			score = (num1*100)/num2;
			console.log("Is "+num1+"/"+num2+" a passing score?");			
			return score;
		}
		let passingScore = thePassingScore(38, 50);
		let isPassingScore = passingScore >= 75;
		console.log(isPassingScore);
